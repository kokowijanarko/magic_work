@extends('adminlte::page')

@section('title', 'Magic Work')

@section('content_header')
<H1>Magic</H1>
@stop

@section('content')
<div class="container-fluid">
    @if ($errors->first())
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                {!! $errors->first() !!}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Submit Form</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ action('GenerateReportController@process_report') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="text">Token</label>
                                    <input type="text" class="form-control" id="token" name="token" placeholder="Bearer Token" value="{{ $token ?? '' }}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tracking ID</label>
                                    <textarea class="form-control" rows="5" id="tracking_id" name="tracking_id" placeholder="Enter Your tracking ID here. seperate with semicolon (;). exemple: tracking_id_1; tracking_id_2;....">{{ $tracking_id ?? ''}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Magic Show</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Tracking id</th>
                                <th>Status Paket</th>
                                <th>Detail</th>
                                <th>Date Solved</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($report ?? '')
                            @foreach ($report as $val)

                            <tr>
                                <td>{!! $val->tracking_id !!}</td>
                                <td>{!! $val->status_paket !!}</td>
                                <td>{!! $val->wording !!}</td>
                                <td>{!! $val->completed_date !!}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
@stop

@section('js')
<!-- DataTables Style -->
<link rel="stylesheet" href="{{ asset('vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

<!-- DataTables -->
<script src="{{ asset('vendor/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('vendor/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>

<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
            "ordering": false,
            "paging": false,
            "searching": true,
            "info": true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'print'
            ],
        });


    });
</script>
@stop
