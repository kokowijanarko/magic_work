<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\ResumeOrder;
use App\Models\Search;
use App\Models\ParcelEvents;
use App\Models\Report;
use Illuminate\Http\Request;

//use Symfony\Component\HttpFoundation\Request;

class GenerateReportController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function main(Request $request)
    {
        //$data = $request->session();
        $data = $request->session()->get('data') == null ? [] : $request->session()->get('data');
        //$data = Session::get('data');

        //dd($data);
        return view("report.show", $data);
    }

    public function process_report(Request $request)
    {
        $input =  $request->all();

        // $tracking_id = "NVIDDPLIF200501188";
        // $token = "dTPqeOIUE41ElCNMnYszdSmGq9mANJ8H";

        $token = $input["token"];
        $tracking_id_str = $input["tracking_id"];
        //dd($tracking_id_str, preg_split('/\n|\r\n?/', $tracking_id_str));
        $tracking_id = [];

        $tracking_id_split= preg_split('/\n|\r\n?/', $tracking_id_str);

        foreach($tracking_id_split as $key=>$val){
           $trk_id = explode(";", $val);
           //dd($trk_id);
           $trek_id_up = strtoupper($trk_id[0]);
           $trek_id_trim = trim($trek_id_up);
           $tracking_id[$key]=[
                "tracking_id" => $trek_id_trim,
                "category"=> isset($trk_id[1]) ? $trk_id[1] : ""
           ];
        }

        //dd($tracking_id);


        // if ($tracking_id_str) {
        //     $tracking_id_str_preg = preg_replace('/\s+/', '', $tracking_id_str);
        //     $tracking_id_str_to_up = strtoupper($tracking_id_str_preg);
        //     $tracking_id_array = explode(';', $tracking_id_str_to_up);
        //     $tracking_id_clean = array_filter($tracking_id_array);
        //     $tracking_id = array_unique($tracking_id_clean);
        // }


        //dd($input, $token, $tracking_id);

        foreach ($tracking_id as $val) {
            $search = new Search();
            $order = new ResumeOrder();
            $events = new ParcelEvents();
            $report = new Report();
            $data_search = $search->getSearch($token, $val['tracking_id']);
            //dd($data_search->nvErrorCode);
            if(isset($data_search->nvErrorCode)){
                $data = [
                    "tracking_id" =>  $input["tracking_id"],
                    "token" => $token
                ];
               // $request->session()->flash('msg', "<strong>". $token. "</strong> ". $data_search->nvErrorCode);
                $request->session()->flash('data', $data);

                // return redirect()->action('GenerateReportController@main', $data)->withErrors(["<strong>". $token. "</strong> ". $data_search->nvErrorCode]);
                return redirect()->action('GenerateReportController@main')->withErrors(["<strong>". $token. "</strong> ". $data_search->nvErrorCode]);

                die();
            }

            if (isset($data_search->search_data[0]->order->id)) {
                $id = $data_search->search_data[0]->order->id;
                $data_order = $order->getOrder($token, $id);
                $data_events = $events->getEvents($token, $id);
                $data_report[] = $report->result($val['tracking_id'], $val['category'], $data_search, $data_order, $data_events);
            } else {
                // dd($data_search);
                // die;
                $data_report[] = $report->result($val['tracking_id'],  $val['category'], "Tracking_id not found", "Tracking_id not found", "Tracking_id not found");
            }
        }

        $data = [
            "report" => $data_report,
            "tracking_id" =>  $input["tracking_id"],
            "token" => $token
        ];
        $request->session()->flash('data', $data);
        return redirect()->action('GenerateReportController@main');
    }
}
