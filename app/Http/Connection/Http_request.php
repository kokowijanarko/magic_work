<?php

namespace App\Http\Connection;

use Illuminate\Database\Eloquent\Model;

class Http_request extends Model
{
    function con($url, $token, $payload = NULL)
    {
        set_time_limit(300);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_SSL_VERIFYPEER => false,
            // CURLOPT_POST => TRUE,
            // CURLOPT_POSTFIELDS=> $payload,

            //           CURLOPT_POSTFIELDS => $transaction,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json, text/plain, */*",
                "Authorization: Bearer " . $token,
                "content-type: application/json",
                "Accept-Encoding: gzip, deflate, br",
                "Connection: keep-alive",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"
            ),
        ));

        if ($payload != null) {
            curl_setopt_array($curl, array(
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $payload
            ));
        }

        //var_dump($curl);die();

        $response = curl_exec($curl);
        $err = curl_error($curl);
        // var_dump($response, $err);die();
        curl_close($curl);

        return $response;
    }
}
