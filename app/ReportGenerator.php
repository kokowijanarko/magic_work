<?php

namespace App;

class ReportGenerator
{
    private $status_paket;
    private $wording;
    private $date_complete;
    private $tracking_id;

    static function get_report($status_paket, $wording, $date_complete, $tracking_id){
        $result = [
            "tracking_id" => $tracking_id,
            "status_paket" => $status_paket,
            "wording" => $wording,
            "date_complete" => $date_complete
        ];
        return $result;
    }

}
