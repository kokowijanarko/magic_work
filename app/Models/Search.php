<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Connection\Http_request;

class Search extends Model
{

    public function getSearch($token, $tracking_id)
    {
        $url = "https://api.ninjavan.co/id/order-search/search?from=0&size=100";

        $search_data = '{"search_field":{"fields":["tracking_id","stamp_id"],"value":"'.$tracking_id.'","match_type":"full_text"},"search_filters":[],"search_range":null}';
        $result = new Http_request();
        $result = $result->con($url, $token, $search_data);
        // dd(json_decode($result));
        return json_decode($result);
    }
}
