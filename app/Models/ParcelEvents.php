<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Connection\Http_request;

class ParcelEvents extends Model
{

    public function getEvents($token, $id)
    {
        $url = "https://api.ninjavan.co/id/events/1.0/orders/" . $id . "/events";

        $result = new Http_request();
        $result = $result->con($url, $token, NULL);

        return json_decode($result);
    }
}
