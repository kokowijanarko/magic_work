<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Connection\Http_request;

class ResumeOrder extends Model
{

    public function getOrder($token, $id)
    {
        $url = "https://api.ninjavan.co/id/core/orders/".$id."?show_scans=false";
        $result = new Http_request();
        $result = $result->con($url, $token, NULL);

        return json_decode($result);
    }
}
