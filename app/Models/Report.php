<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\ReportGenerator;

class Report extends Model
{

    public function result($tracking_id, $complain_category, $search, $order, $events)
    {
        //dd($order);
        $wording = "";
        $status_paket = "On Proses";
        $date_complete = "";
        $result = (object) array();

        if ($search == "Tracking_id not found") {
            $result->tracking_id = $tracking_id;
            $result->status_paket = "On Proses";
            $result->wording = "Tracking id Tidak ditemukan";
            $result->completed_date = date("n/j/Y");
        } else {

            if (isset($order->granularStatus)) {
                $granular_status = $order->granularStatus;
            } elseif (isset($order->granular_status)) {
                $granular_status = $order->granular_status;
            }


            //var_dump($granular_status);die();

            if ($granular_status == "COMPLETED") {
                $date = date("n-j-Y", strtotime($order->updatedAt));
                // $date_complete = $date;
                $date_complete = date("n/j/Y");
                $wording = "Completed delivery (" . $date . ")";
                $status_paket = "Solved";
                $date_complete = date("n/j/Y");
            } elseif ($granular_status == "RETURNED_TO_SENDER") {
                $wording = "Paket RTS. ";
                $status_paket = "Solved";
                $date_complete = date("n/j/Y");
            } elseif ($granular_status == "ON_HOLD") {
                $wording = "Paket On Hold. ". $this->get_reason($events, $order, $granular_status);
            } elseif ($granular_status == "CANCELLED") {
                $wording = "Paket Dibatalkan. ";
                $status_paket = "Solved";
                $date_complete = date("n/j/Y");
            } else {
                $wording = $this->get_reason($events, $order, $granular_status);
                if($wording == '')
                {
                    $wording = "Sedang dalam pengiriman " . $order->destinationHub;
                }
            }

            //$result = $result->get_report($status_paket, $wording, $date_complete, $tracking_id);

            $result->tracking_id = $tracking_id;
            $result->status_paket = $status_paket;
            $result->wording = $wording;
            $result->completed_date = $date_complete;
        }

        return $result;
    }

    function get_reason($events, $order, $granular_status)
    {

        $attemp_wording = "";
        $missing_indic_wording = "";
        $missing_wording = "";
        $reject_wording = "";
        $rts_wording = "";

        $is_reject = false;
        $is_missing = false;
        $is_missing_indic = false;
        $is_rts = false;

        $address_ver_act = 0;
        //print_r(count($events->data));die();
        foreach ($events->data as $event) {
            //address verification
            // if($event->type == "VERIFY_ADDRESS")
            // {
            //     if($address_ver_act > 0)
            //     {
            //         $reason .= "terdapat perubahan alamat. ";
            //         break;
            //     }
            //     $address_ver_act++;
            // }

            //indikasi missing
            if ($event->type == "TICKET_CREATED") {
                // print_r($event->data->ticket_type);
                // print_r("</br>");
                if (isset($event->data->ticket_type)) {
                    if ($event->data->ticket_type == "MISSING") {
                        $is_missing = true;
                    }
                    if ($event->data->ticket_type == "PARCEL EXCEPTION") {
                        if (isset($event->data->ticket_sub_type)) {
                            if ($event->data->ticket_sub_type == "CUSTOMER REJECTED") {
                                $is_reject = true;
                            } else {
                                $is_missing_indic = true;
                            }
                        }
                    }
                }
            }

            if ($event->type == "RTS") {
                $is_rts = true;
            }
        }


        // die();
        //attempt
        $attemp = 0;
        foreach ($order->transactions as $transactions) {
            if ($transactions->type == "DELIVERY" && $transactions->status == "FAIL") {
                $attemp++;
            }
        }
        if ($is_missing == TRUE) {
            $missing_wording .= " Paket Missing. ";
            $attemp_wording = "";
            $missing_indic_wording = "";
            $missing_wording = "";
        }
        if ($is_missing_indic == true) {
            $missing_indic_wording .= " Indikasi Missing, Sedang di investigasi oleh tim recovery. ";
        }

        if ($attemp > 0 && $attemp <= 3) {
            $attemp_wording .= "Sudah berusaha dikirim sebanyak " . $attemp . " kali.";
        } elseif ($attemp == 4) {
            $attemp_wording .= "Sudah berusaha dikirim sebanyak " . $attemp . " kali (Mencapai batas maksimal percobaan pengiriman).";
            $missing_indic_wording = "";
            $missing_wording = "";
        }

        if ($is_reject == true) {
            $reject_wording .= " Paket sudah dikirimkan tetapi gagal karena customer membatalkan pesanan. ";
            $attemp_wording = "";
            $missing_indic_wording = "";
            $missing_wording = "";
        }

        if ($granular_status != "RETURNED_TO_SENDER" && $is_rts == true) {
            $rts_wording .= "Paket proses RTS. ";
            $attemp_wording = "";
            $missing_indic_wording = "";
            $missing_wording = "";
        }


        $reason = $reject_wording. $rts_wording . $attemp_wording . $missing_indic_wording . $missing_wording;
        return $reason;
    }
}
